/**
 * validation for the contact page. 
 */
$('.ui.form')
    .form({
      firstName: {
        identifier: 'firstName',
        rules: [
          {
            type: 'empty',
            prompt: 'please enter your first name',
          },
        ],
      },
      lastName: {
        identifier: 'lastName',
        rules: [
          {
            type: 'empty',
            prompt: 'please enter your last name',
          },
        ],
      },
      email: {
        identifier: 'email',
        rules: [
          {
            type: 'empty',
            prompt: 'please enter your email',
          },
        ],
      },
      messageTxt: {
        identifier: 'messageTxt',
        rules: [
          {
            type: 'empty',
            prompt: 'You have not entered any message',
          },
        ],
      },
    });
