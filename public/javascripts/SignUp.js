/**
 * validation for sign up
 * @returns {Boolean}
 */

function checkTerms() {
  var terms = document.getElementById('terms').checked;
  if (terms === false) {
    alert('Please accept the terms and conditions before continuing');
    return false;
  }
};

$('.ui.form')
    .form({
      firstName: {
        identifier: 'firstName',
        rules: [
          {
            type: 'empty',
            prompt: 'please enter your first name',
          },
        ],
      },
      lastName: {
        identifier: 'lastName',
        rules: [
          {
            type: 'empty',
            prompt: 'please enter your last name',
          },
        ],
      },
      email: {
        identifier: 'email',
        rules: [
          {
            type: 'empty',
            prompt: 'please fill in your email',
          },
        ],
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type: 'empty',
            prompt: 'Please fill in a password',
          },
        ],
      },
      address: {
        identifier: 'address',
        rules: [
          {
            type: 'empty',
            prompt: 'Please fill in your address',
          },
        ],
      },
    });
