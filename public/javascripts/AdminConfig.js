/**
 * javascript to control elements of admin config page
 * ajax implemented to allow user to delete a tenant or landlord without page refresh
 * deletes the tenant/landlord from dropdown using this javascript
 */

$('.ui.dropdown').dropdown();

$('.ui.form.landlord')
    .form({
    fields: {
      lordid: {
        identifier: 'lordid',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a Landlord',
          },
        ],
      },
    },
    
    onSuccess: function(event, fields){
    	deleteLord();
    	event.preventDefault();
    }
});

function deleteLord(){
  var formData = $('.ui.form.landlord input').serialize();
  $.ajax({
    type: 'POST',
	url: '/Admins/deleteLandlord',
	data: formData,
	  
	  success: function(response){
			let email = $('#landlordDD').dropdown('get value');
	        removeItemLandlordDropdown(email);
	        ADMIN_MAP.updateMarkers(response);
	   }
   });
};
function removeItemLandlordDropdown(email) {
    let $obj = $('.item.lord');
    console.log($obj);
    for (let i = 0; i < $obj.length; i += 1) {
      if($obj[i].getAttribute('data-value').localeCompare(email) == 0) {
        $obj[i].remove();
        $('#landlordDD').dropdown('clear');
        break;
      }
    }
  }

$('.ui.form.tenant')
  .form({
	fields: {
      tenid: {
        identifier: 'tenid',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a Tenant',
          },
        ],
      },
	},
      onSuccess: function(event, fields){
    	  deleteTen();
    	  event.preventDefault();
   }
});    
  
function deleteTen(){
	  var formData = $('.ui.form.tenant input').serialize();
	  $.ajax({
	    type: 'POST',
		url: '/Admins/deleteTenant',
		data: formData,
		  
		  success: function(response){
				let email = $('#TenantDD').dropdown('get value');
		        removeItemTenantDropdown(email);
		        ADMIN_MAP.updateMarkers(response);
		   }
	   });
	};
	function removeItemTenantDropdown(email) {
	    let $obj = $('.item.ten');
	    for (let i = 0; i < $obj.length; i += 1) {
	      if($obj[i].getAttribute('data-value').localeCompare(email) == 0) {
	        $obj[i].remove();
	        $('#TenantDD').dropdown('clear');
	        break;
	      }
	    }
	  }


    

