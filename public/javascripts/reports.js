/**
 * validation for report page
 */
$('.ui.dropdown').dropdown();
$('.ui.form')
    .form({
      rentedStatus: {
        identifier: 'rentedStatus',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select if vacant or rented',
          },
        ],
      },
      residenceType: {
        identifier: 'residenceType',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a residence type',
          },
        ],
      },
      sortDirection: {
        identifier: 'sortDirection',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select an option',
          },
        ],
      },
    });
