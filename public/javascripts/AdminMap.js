/**
 * controls map on admin page
 */

const ADMIN_MAP = (function () {
  var map;
  latlngStr = [];
  const markerLatLng = [];
  let marker;
  const markers = [];
  
  /**
   * uses ajax call to populate an [] in javascript.
   */
  function initialize()
  {
    $(function () {
      $.get('/Admins/geolocation', function (data) {
      }).done(function (data) {
        $.each(data, function (index, geoObj)
        {
          console.log(geoObj[0] + ' ' + geoObj[1] + ' ' + geoObj[2] + ' ' + geoObj[3]);
        });
        positionMarkers(data);
      });
    });
  }

  /**
   * loops through data and adds to []
   * passes [] to basic map
   * @param data
   */
  function positionMarkers(data)
  {
    $.each(data, function (index, geoObj) {
      latlngStr.push(geoObj);
    });
    map(latlngStr);
  }

  /**
   * initializes map and adds markers to map
   * @param latlngStr
   */
  function map(latlngStr) {
    var center = new google.maps.LatLng(53.3702463, -7.4164152);
    const infowindow = new google.maps.InfoWindow();

    var mapProp = {
      center: center,
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    var mapDiv = document.getElementById('map');
    map = new google.maps.Map(mapDiv, mapProp);
    mapDiv.style.width = '100%';
    mapDiv.style.height = '500px';

    for (i = 0; i < latlngStr.length; i++)
    {
      marker = new google.maps.Marker({
        position: getLatLng(latlngStr[i]),
        map: map,
      });
      google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
          infowindow.setContent('Eircode ' + latlngStr[i][0] + ' : ' + latlngStr[i][3]);
          infowindow.open(map, marker);
        };
      })(marker, i));

      markers.push(marker);
    }
  }

  /**
   * helper method to get lat lng for placing markers
   * @param str
   * @returns {google.maps.LatLng}
   */
  function getLatLng(str)
  {
    const lat = Number(str[1]);
    const lon = Number(str[2]);
    return new google.maps.LatLng(lat, lon);
  }

  /**
   * when a landlord is deleted, the markers are updated(deleted)
   * when a tenant is deleted, the markers info window is updated to "no tenant"
   * @param data
   */
  function updateMarkers(data)
  {
    removeMarkers();
    latlngStr = [];
    $.each(data, function (index, geoObj)
    {
      latlngStr.push(geoObj);
    });

    const infowindow = new google.maps.InfoWindow();

    for (i = 0; i < latlngStr.length; i++)
    {
      marker = new google.maps.Marker({
        position: getLatLng(latlngStr[i]),
        map: map,
      });
      google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
          infowindow.setContent('Eircode ' + latlngStr[i][0] + ' : ' + latlngStr[i][3]);
          infowindow.open(map, marker);
        };
      })(marker, i));

      markers.push(marker);
    }
  }

  /**
   * removes markers that should no longer be there because of a deletion of a landlord
   */
  function removeMarkers()
  {
    for (i = 0; i < markers.length; i += 1)  {
      markers[i].setMap(undefined);
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  return {
    updateMarkers: updateMarkers,
  };

}());
