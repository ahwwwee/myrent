/**
 * validation for tenants landing page
 */
$('.dropdown').dropdown();

/**
 * checks the moveout form and if clicked with start method
 */
$('.ui.form.moveOut').form({
  onSuccess: function (event) {
    moveOut();
    event.preventDefault();
  },
});

/**
 * makes tenant homeless
 * allows them to move into another tenancy
 * if a tenant is not in a residence the button cannot be clicked
 */
function moveOut() {
  if($('#existeircode').val() !== ""){
    var formData = $('.ui.form.segment.moveOut input').serialize();
    var OldRental = document.getElementById('exist_eircode').value;
    var resid = document.getElementById('existeircode').value;
    $.ajax({
    type: 'POST',
    url: '/Tenants/moveOut',
    data: formData,

    success: function(response) {
      let newMenuItem = anything(OldRental, resid)
      $('#exist_eircode').val('No Residence');
      $('#existeircode').val("");
      $('.menu.moveIn').append(newMenuItem);
      ADMIN_MAP.updateMarkers(response);
    },
  });
 }
}
/**
 * adds old rented residence to dropdown box to be selected as a residence to move into
 * @param OldRental
 * @param resid
 * @returns {String}
 */
function anything(OldRental, resid){
	return '<div id="resid" class="item eircode"' + ' ' + 'data-value="' + resid + '">' + OldRental + '</div>';
}

/**
 * checks the movein form and if clicked with start method
 */
$('.ui.form.moveIn').form({
  fields: {
    resid: {
      identifier: 'resid',
      rules: [{
        type: 'empty',
        prompt: 'Select a vacant residence',
      },],
    },
  },

  onSuccess: function (event, fields) {
    moveIn();
    event.preventDefault();
  },
});

/**
 * makes a residence have said tenant in its reference
 * if tenant is in a residence the button cannot be clicked
 */
function moveIn() {
	if($('#existeircode').val() === ""){	
  var formData = $('.ui.form.moveIn input').serialize();
  let $eircodeNewRental = $('#moveHouse').dropdown('get text');
  $.ajax({
    type: 'POST',
    url: '/Tenants/moveIn',
    data: formData,

    success: function (response) {
    	let $resid = $('#moveHouse').dropdown('get value');
      updateTenancy($eircodeNewRental, $resid);
      ADMIN_MAP.updateMarkers(response);
    },
  });
	}
}

/**
 * updates eircode field and resid field. 
 * takes the residence being moved into out of the dropdown box
 * @param NewRental
 * @param $resid
 */
function updateTenancy(NewRental, $resid){
  let $obj = $('.item.eircode');
  for (let i = 0; i < $obj.length; i++) {
    if ($obj[i].getAttribute('data-value').localeCompare($resid) == 0) {
    	$obj[i].remove();
      $('#moveHouse').dropdown('clear');
      break;
    }
  }
  $('#exist_eircode').val(NewRental);
  $('#existeircode').val($resid);
}

