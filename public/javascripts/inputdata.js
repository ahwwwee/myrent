/**
 * validation for the inputdata page
 */

$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();
$('.ui.form')
    .form({
      geolocation: {
        identifier: 'geolocation',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a location on the map',
          },
        ],
      },
      eircode: {
        identifier: 'eircode',
        rules: [
          {
            type: 'empty',
            prompt: 'Please find your eircode',
          },
        ],
      },
      rent: {
        identifier: 'rent',
        rules: [
          {
            type: 'empty',
            prompt: 'Please input Rent amount',
          },
        ],
      },
      numberBedrooms: {
        identifier: 'numberBedrooms',
        rules: [
          {
            type: 'number',
            type: 'empty',
            prompt: 'Please input number of Bedrooms',
          },
        ],
      },
      numberBathrooms: {
        identifier: 'numberBathrooms',
        rules: [
          {
            type: 'number',
            type: 'empty',
            prompt: 'Please input number of Bathrooms',
          },
        ],
      },
      area: {
        identifier: 'area',
        rules: [
          {
            type: 'number',
            type: 'empty',
            prompt: 'Please input Area, in m2',
          },
        ],
      },
      residenceType: {
        identifier: 'residenceType',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a residence type',
          },
        ],
      },
    });
