/**
 * validation for login pages
 */
$('.ui.form')
    .form({
      email: {
        identifier: 'email',
        rules: [
          {
            type: 'empty',
            prompt: 'Enter your email to Sign in',
          },
        ],
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter your password',
          },
        ],
      },
    });