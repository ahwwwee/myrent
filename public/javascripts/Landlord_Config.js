/**
 * validation for landlord config page
 */
$('.ui.dropdown').dropdown();
$('.ui.form')
.form({
    resid: {
      identifier: 'resid',
      rules: [
        {
        	type: 'empty',
        	prompt: 'this should never be seen',
        },
      ],
    },
});