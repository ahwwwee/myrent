/**
 * map used on tenant page
 * uses ajax to populate arrays for markers and information for polygon search report results
 */

let marker;
latlngStr = [];
const pos = [];
let posIndex = 0;
var map;
let markers = [];
const ADMIN_MAP = (function () {

  /**
   * uses ajax call to populate an [] in javascript.
   */
  function initialize() {
      $.get('/Tenants/geolocationTenant', function (data) {
      }).done(function (data) {
        $.each(data, function (index, geoObj)
        {
          console.log(geoObj[0] + ' ' + geoObj[1] + ' ' + geoObj[2] + ' ' + geoObj[3]);
        });

        positionMarkers(data);
      });
  }

  /**
   * loops through data and adds to []
   * passes [] to basic map
   * @param data
   */
  function positionMarkers(data)
  {
    $.each(data, function (index, geoObj) {
      latlngStr.push(geoObj);
    });

    basicMap(latlngStr);
  }

  /**
   * helper method to get lat lng for placing markers
   * @param str
   * @returns {google.maps.LatLng}
   */
  function getLatLng(str)
  {
    const lat = Number(str[1]);
    const lon = Number(str[2]);
    return new google.maps.LatLng(lat, lon);
  }

  /**
   * initializes map and adds markers to map
   * @param latlngStr
   */
  function basicMap(latlngStr) {
    var center = new google.maps.LatLng(53.3702463, -7.4164152);

    const mapProp = {
      center: center,
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    mapDiv = document.getElementById('map');
    map = new google.maps.Map(mapDiv, mapProp);
    mapDiv.style.width = '100%';
    mapDiv.style.height = '500px';

    for (i = 0; i < latlngStr.length; i++)
    {
      title = 'Eircode ' + latlngStr[i][0] + ' : ' + latlngStr[i][3];
      marker = new google.maps.Marker({
        position: getLatLng(latlngStr[i]),
        map: map,
        title: title,
      });
      markers.push(marker);
    }
  }

  /**
   * when a landlord is deleted, the markers are updated(deleted)
   * when a tenant is deleted, the markers info window is updated to "no tenant"
   * @param data
   */
  function updateMarkers(data)
  {
    removeMarkers();
    markers = [];
    latlngStr = [];
    $.each(data, function (index, geoObj)
    {
      latlngStr.push(geoObj);
    });

    for (i = 0; i < latlngStr.length; i++)
    {
      title = 'Eircode ' + latlngStr[i][0] + ' : ' + latlngStr[i][3];
      marker = new google.maps.Marker({
        position: getLatLng(latlngStr[i]),
        map: map,
        title: title,
      });
      markers.push(marker);
    }
  }

  /**
   * removes markers that should no longer be there because of a deletion of a landlord
   */
  function removeMarkers()
  {
    for (i = 0; i < markers.length; i += 1)  {
      markers[i].setMap(undefined);
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  return {
    updateMarkers: updateMarkers,
  };
}());

/**
 * allows the onclick="" on button to work
 */
function start() {
  $('#markertable').empty();
  listenerHandler = google.maps.event.addListener(map, 'click', function (e) {
    pos[posIndex] = e.latLng;
    if (posIndex > 0) {
      polyline(posIndex - 1, posIndex);
    }

    posIndex += 1;
  });

}

/**
 * allows the onclick="" on button to work
 */
function stop() {
  polyline(pos.length - 1, 0);
  drawPolygon();
  google.maps.event.removeListener(listenerHandler);
  listenerHandler = null;
};

/**
 * allows the onclick="" on button to work
 */
function filter() {
  latlng = [];
  for (let i = 0; i < latlngStr.length; i += 1) {
    const point = new google.maps.LatLng(latlngStr[i][1], latlngStr[i][2]);
    if (google.maps.geometry.poly.containsLocation(point, polygon)) {
      markers[i].setVisible(true);
      latlng.push(latlngStr[i]);
    } else {
      markers[i].setVisible(false);
    }
  }

  populateTable(latlng);
}

/**
 * loops through data and adds to []
 * passes [] to basic map
 * @param latlng
 */
function populateTable(latlng)
{
  $.each(latlng, function (i, val) {
    populateTableRow(val);
  });
}

/**
 * adds rows to table on page
 * @param data
 */
function populateTableRow(data)
{
  const id = '<td>' + data[4] + '</td>';
  const landlord   = '<td>' + data[5]  + '</td>';
  const date = '<td>' + data[6]  + '</td>';
  const tenant = '<td>' + data[3]  + '</td>';
  const rent = '<td>' + data[7]  + '</td>';
  const bedrooms = '<td>' + data[8]  + '</td>';
  const bathrooms = '<td>' + data[9]  + '</td>';
  const area = '<td>' + data[10]  + '</td>';
  $('#markertable').append('<tr>' + id + landlord + date + tenant + rent + bedrooms + bathrooms + area + '</tr>');
}

/**
 * refreshes page
 */
function reset() {
  location.reload();
}

/**
 * creates a polyline for a part of the polygon
 * @param prevIndex
 * @param index
 */
function polyline(prevIndex, index) {
  const coords = [
    new google.maps.LatLng(pos[prevIndex].lat(), pos[prevIndex].lng()),
    new google.maps.LatLng(pos[index].lat(), pos[index].lng()),];

  const line = new google.maps.Polyline({
    path: coords,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2,
  });
  line.setMap(map);
}

/**
 * when finished with the polylines and stop is pressed the polygon will close the last gap
 */
function drawPolygon() {
  const lineCoords = [];
  for (let j = 0; j < pos.length; j += 1) {
    console.log(pos[j].lat + ' ' + pos[j].lng);
    lineCoords[j] = new google.maps.LatLng(pos[j].lat(), pos[j].lng());
  }

  lineCoords[pos.length] = new google.maps.LatLng(pos[0].lat(), pos[0].lng());

  polygon = new google.maps.Polyline({
    path: lineCoords,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2,
  });

  polygon.setMap(map);
  google.maps.event.clearListeners(map, 'click');
}
