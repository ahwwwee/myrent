/**
 * map used on inputData page.
 */
function initialize() {
  let map;
  let marker;
  const latlng = new google.maps.LatLng(53.347298, -6.268344);

  const mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(53.347298, -6.268344),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  };
  let mapDiv = document.getElementById('map_canvas');
  map = new google.maps.Map(mapDiv, mapOptions);
  mapDiv.style.width = '100%';
  mapDiv.style.height = '500px';
  marker = new google.maps.Marker({
    map: map,
    draggable: true,
    position: latlng,
    title: 'Drag and drop on your property!',
  });

  marker.setMap(map);
  google.maps.event.addListener(marker, 'dragend', function () {
    let latLng = marker.getPosition();
    let latlong = latLng.lat().toString().substring(0, 10) + ',' + latLng.lng().toString().substring(0, 10);
    $('#geolocation').val(latlong);
    map.setCenter(latLng);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
