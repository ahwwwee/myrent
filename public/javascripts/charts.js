/**
 * to populate the chart on the charts page.
 * two array lists are populated through a jquery request 
 */
var residenceLord = [];
var residenceRent = [];
var i = 0;
onLoad = first();

function first(){
   $('.resName').each(function () {
    residenceLord[i] = $(this).attr('value');
    i++;
  });


  i = 0
  $('.resRent').each(function () {
    residenceRent[i] = $(this).attr('value');
    i++;
  });
  render(residenceLord, residenceRent);
}

/**
 * populates the chart. 
 * @param residenceLord
 * @param residenceRent
 */
function render(residenceLord, residenceRent)
{
  let dps = dataArray(residenceLord, residenceRent);

  $('#chartContainer').CanvasJSChart({
    title: {
      text: 'Landlord rent rolls as % income all portfolios',
      fontSize: 24,
    },
    axisY: {
      title: 'Rental income %',
    },
    legend: {
      verticalAlign: 'center',
      horizontalAlign: 'right',
    },
    data: [{
      type: 'pie',
      showInLegend: true,
      toolTipContent: '{label} <br/> {y} %',
      indexLabel: '{y} %',
      dataPoints: dps,
    },],
  });
}

/**
 * organising information taken in
 * @param names
 * @param rents
 * @returns
 */
function dataArray(names, rents)  {
  let data = [];
  for (let i = 0; i < names.length; i += 1)  {
    data.push({ label: names[i],
      y: Number(Math.round((rents[i]) * 10) / 10),
      legendText: names[i], });
  }

  return data;
}
