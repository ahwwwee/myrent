 package models;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import utils.LatLng;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import controllers.Tenants;
import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Residence extends Model
{
	
	public String geolocation;
	public int rent;
	public int numberBedrooms;
	public String residenceType;
	public int numberBathrooms;
	public int area;
	public String eircode;
	public Date date;
	
	@ManyToOne
	public Landlord landlord;
	
	@OneToOne
	public Tenant tenant;
	
	/**
	 * constructor for residence
	 * @param landlord
	 * @param tenant
	 * @param geolocation
	 * @param rent
	 * @param numberBedrooms
	 * @param residenceType
	 * @param numberBathrooms
	 * @param area
	 */
    public Residence(Landlord landlord, String geolocation, String eircode, int rent, int numberBedrooms, String residenceType, int numberBathrooms, int area)
    {
    	this.geolocation = geolocation;
    	this.eircode = eircode;
    	this.rent = rent;
    	this.numberBedrooms = numberBedrooms;
    	this.residenceType = residenceType;
    	this.numberBathrooms = numberBathrooms;
    	this.area = area;
    	this.landlord = landlord;
    	date = new Date();
    }
    
    /**
     * method used in other classes to get residence geolocation in lat lng format
     * @return
     */
    public LatLng getGeolocation()
    {
    	return LatLng.toLatLng(geolocation);
    }
	
}