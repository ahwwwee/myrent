package models;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Tenant extends Model
{
	public String firstName;
    public String lastName;
    public String email;
    public String password;
    public Date date; 
    
    @OneToOne(mappedBy="tenant")
    public Residence res;
   
    /**
     * constructor for tenant
     * @param firstName
     * @param lastName
     * @param email
     * @param password
     */
    public Tenant(String firstName, String lastName, String email, String password)
    {
    	this.firstName = firstName;
    	this.lastName = lastName;
    	this.email = email;
    	this.password = password;
    	date = new Date();

    } 

    /**
     * allows methods in other classes to find admin by email
     * @param email
     * @return
     */
    public static Tenant findByEmail(String email)
    {
    	return find("email", email).first();
    }
    
    /**
     * used by authenticate method in controller
     * @param password
     * @return
     */
    public boolean checkPassword(String password)
    {
    	return this.password.equals(password);
    }   
}