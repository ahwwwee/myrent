package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class contactModel extends Model
{
	public String firstName;
	public String lastName;
	public String email;
	public String messageTxt;
	
	/**
	 * constructor for contactModel
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param messageTxt
	 */
	public contactModel(String firstName, String lastName, String email, String messageTxt)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.messageTxt = messageTxt;
	}
}