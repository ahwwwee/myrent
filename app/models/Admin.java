package models;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Admin extends Model
{
    public String email;
    public String password;
    
    /**
     * constructor for admin
     * @param email
     * @param password
     */
    public Admin()
    {
    	email = "admin@witpress.ie";
    	password = "secret";
    }

    /**
     * allows methods in other classes to find admin by email
     * @param email
     * @return
     */
    public static Admin findByEmail(String email)
    {
    	return find("email", email).first();
    }
    
    /**
     * used by authenticate method in controller
     * @param password
     * @return
     */
    public boolean checkPassword(String password)
    {
    	return this.password.equals(password);
    }
    
}