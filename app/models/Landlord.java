package models;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Landlord extends Model
{
	public String firstName;
    public String lastName;
    public String email;
    public String password;
    public Date date; 
    public String address;
    
    /**
     * constructor for landlord
     * @param firstName
     * @param lastName
     * @param email
     * @param password
     * @param address
     */
    public Landlord(String firstName, String lastName, String email, String password, String address)
    {
    	this.firstName = firstName;
    	this.lastName = lastName;
    	this.email = email;
    	this.password = password;
    	this.address = address;
    	date = new Date();  
    }
    
    /**
     * allows methods in other classes to find admin by email
     * @param email
     * @return
     */
    public static Landlord findByEmail(String email)
    {
    	return find("email", email).first();
    }
    
    /**
     * used by authenticate method in controller
     * @param password
     * @return
     */
    public boolean checkPassword(String password)
    {
    	return this.password.equals(password);
    }
    
}