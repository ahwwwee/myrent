package controllers;

import play.*;

import play.mvc.*;
import utils.LatLng;

import java.sql.Array;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import models.*;

public class Admins extends Controller
{
  /**
   * to render login page
   */
  public static void login()
  {
    render();
  }

  /**
   * to logout admin
   */
  public static void logout()
  {
    session.remove("logged_in_adminid");
    Landlords.welcome();
  }
  
  /**
   * renders config page. if admin is logged in otherwise reroutes to login page
   * adds lists of Tenants and Landlords and Residences to the page
   */
  public static void config()
  {
	  if(session.get("logged_in_adminid") != null)
	  {
		  List<Landlord> lords = Landlord.findAll();
		  List<Tenant> tens = Tenant.findAll();
		  List<Residence> res = Residence.findAll();
	    render(lords, tens, res);
	  }
	  login();	  
  }
  
  /**
   * renders reports page.
   * takes in @param sortBy, and uses this to determine 
   * how the list of residence rendered with be displayed
   */
  public static void reports(String sortBy)
  {
	  List<Residence> Resi = Residence.findAll();
	  List<Residence> res = new ArrayList();
	  if(sortBy == null){
		  res = Residence.findAll();
	  }
	  
	  else if(sortBy.equals("yes") || sortBy.equals("no")){
        for(Residence r : Resi){
		  if(sortBy.equals("yes") && r.tenant != null){
			  res.add(r);
		  }
		  else if(sortBy.equals("no") && r.tenant == null){
			  res.add(r);
		  }
        }
	  }
	  
	  else if(sortBy.equals("house") || sortBy.equals("flat") || sortBy.equals("studio")){
		  for(Residence r : Resi){
			  if(sortBy.equals(r.residenceType)){
				  res.add(r);
			  }
		  }
	  }
	  else if(sortBy.equals("ascending") || sortBy.equals("descending")){
		  res = Residence.findAll();
		  if(sortBy.equals("descending")){
		    Collections.sort(res, new rentSortDecending());
		  }
		  else if(sortBy.equals("ascending")){
			Collections.sort(res, new rentSortAscending());
		  }
		  
	  }
      render(res);
  }
  
  /**
   * renders charts page
   * adds lists of residences and landlords to page
   * also adds an array with percent of each landlords total rent within all of the landlords
   */
  public static void charts()
  {
	  List<Residence> res = Residence.findAll();
	  List<Landlord> lords = Landlord.findAll();
	  
	  ArrayList<String> rents = new ArrayList();
	  Logger.info(rents + "");
      for(Landlord l : lords){
		  double lordRentTotal = 0;
		  for(Residence r : res){
			  if(r.landlord == l){
				lordRentTotal += r.rent;
			  }
		  }
		  String percentachieved = String.valueOf(lordRentTotal * 100 / getTotalRent());
		  rents.add(percentachieved);		  
	  }
	  render(res, lords, rents);
  }
  
  /**
   * helper method for charts() render
   * gets all residences rent amounts to get total rent amount
   * @return
   */
  public static int getTotalRent()
  {
	  List<Residence> res = Residence.findAll();
	  int amount = 0;
	  for(Residence r : res){
		  amount += r.rent;
	  }
	  return amount;
  }
  
  /**
   * helper method for reports() render
   * tells reports to sort by rented or by vacant
   * @param rentedStatus
   */
  public static void sortRented(String rentedStatus)
  {	  
	  reports(rentedStatus);
  }
  
  /**
   * helper method for reports() render
   * tells reports to sort by residence type
   * (flat, house, studio)
   * @param residenceType
   */
  public static void sortType(String residenceType)
  {
	  reports(residenceType);
  }
  
  /**
   * helper method for reports() render
   * tells reports to sort by rent amount
   * either assending or decending. 
   * in the report method a comparitor is used
   * @param sortDirection
   */
  public static void sortRent(String sortDirection)
  {	 
	  reports(sortDirection);
  }
  
  /**
   * deletes landlord from the database, 
   * also renders a json object to update marker information on the page using ajax
   * @param lordid
   */
  public static void deleteLandlord(Long lordid)
  {
	  Landlord lord = Landlord.findById(lordid);
	  List<Residence> ress = Residence.findAll();
	  
	  for(Residence r : ress)
	  {
		  if(r.landlord == lord)
		  {
			  r.tenant = null;
			  r.save();
			  r.delete();
		  }
	  }
	  lord.delete();
	  List<Residence> res = Residence.findAll();
	  JSONArray obj = new JSONArray();
	  for(Residence r : res){
		  String tenant = null;
		  if(r.tenant != null){
            tenant = "Tenant: " + r.tenant.firstName + r.tenant.lastName;
		  }
		  else{
			tenant = "No Tenant";
		  }
	  String eircode = r.eircode;
	  String lat = String.valueOf(r.getGeolocation().getLatitude());
	  String lng = String.valueOf(r.getGeolocation().getLongitude());
		  String[] list = {eircode, lat, lng, tenant};
		  obj.add(list);
	  }
	  renderJSON(obj);
  }
  
  /**
   * deletes tenant from the database, 
   * also renders a json object to update marker information on the page using ajax
   * @param lordid
   */
  public static void deleteTenant(Long tenid)
  {
	  Tenant ten = Tenant.findById(tenid);
	  if(ten.res != null)
	  {
	    ten.res.tenant = null;
	    ten.res.save();
	    ten.res = null;
	  }
	  Logger.info("" + ten);
	  ten.delete();
	  List<Residence> res = Residence.findAll();
	  JSONArray obj = new JSONArray();
	  for(Residence r : res){
		  String tenant = null;
		  if(r.tenant != null){
            tenant = "Tenant: " + r.tenant.firstName + r.tenant.lastName;
		  }
		  else{
			tenant = "No Tenant";
		  }
	  String eircode = r.eircode;
	  String lat = String.valueOf(r.getGeolocation().getLatitude());
	  String lng = String.valueOf(r.getGeolocation().getLongitude());
		  String[] list = {eircode, lat, lng, tenant};
		  obj.add(list);
	  }
	  renderJSON(obj);
  }
 
  /**
   * returns a json array to accomidate the ajax used 
   * to populate the markers on the map on configuration page
   */
  public static void geolocation()
  {
	  List<Residence> res = Residence.findAll();
	  JSONArray obj = new JSONArray();
	  for(Residence r : res){
		  String tenant = null;
		  if(r.tenant != null){
            tenant = "Tenant: " + r.tenant.firstName + r.tenant.lastName;
		  }
		  else{
			tenant = "No Tenant";
		  }
	  String eircode = r.eircode;
	  String lat = String.valueOf(r.getGeolocation().getLatitude());
	  String lng = String.valueOf(r.getGeolocation().getLongitude());
		  String[] list = {eircode, lat, lng, tenant};
		  obj.add(list);
	  }
	  renderJSON(obj);
  }

  /**
   * checks that the details put into login page are correct
   * allows user to log in
   * @param email
   * @param password
   */
  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);
    Admin admin = new Admin();
    if ((admin != null) && (admin.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_adminid", admin.id);
      config();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
  
  /**
   * @return the admin that is logged in if there are any 
   */
  public static Admin getCurrentAdmin()
  {
	  String adminId = session.get("logged_in_adminid");
	  if(adminId != null)
	  {
		  Admin logged_in_admin = Admin.findById(Long.parseLong(adminId));
		  return logged_in_admin;
	  }
	  return null;
  }
}