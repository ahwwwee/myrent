package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import javax.persistence.ManyToOne;

import models.*;

public class Contact extends Controller
{
	/**
	 * renders contact index page
	 */
	public static void index()
	{
		render();
	}
	
	/**
	 * renders contact acknowledgment page
	 */
	public static void acknowledgement()
	{
		render();
	}
	
	/**
	 * adds message to the contact model model so that it may be read in the database
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param messageTxt
	 */
	public static void sendMessage(String firstName, String lastName, String email, String messageTxt)
	{
	    contactModel mes = new contactModel(firstName, lastName, email, messageTxt);
		mes.save();
		Contact.acknowledgement();
	}
}