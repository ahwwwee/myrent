package controllers;
import java.util.Comparator;

import models.Residence;

public class rentSortAscending implements Comparator<Residence>
{
  /**
   * Sorts the Residences in parameters by rent variable in assending order
   */
	@Override
  public int compare(Residence a, Residence b)
  {	 
      return Integer.compare (a.rent, b.rent);
  }
}