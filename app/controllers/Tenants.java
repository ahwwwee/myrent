package controllers;

import play.*;
import play.mvc.*;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

import java.util.*;

import org.json.simple.JSONArray;

import models.*;

public class Tenants extends Controller
{
  /**
<<<<<<< HEAD
   * renders sign up page for tenant
   */
  public static void signup()
  {
	  render();
  }
 
  /**
   * renders tenant log in page
   */
  public static void login()
  {
    render();
  }

  /**
   * logs out currently logged in tenant
   * rerouts to welcome page
   */
  public static void logout()
  {
    session.remove("logged_in_tenid");
    
    Landlords.welcome();
  }

  /**
   * renders landing page for tenant if logged in tenant is available
   * renders currenly logged in tenant to the page.
   */

  public static void config()
  {
	  Tenant ten = getCurrentTenant();
   if(ten != null){
	  List<Residence> all = Residence.findAll();
      List<Residence> vacant = new ArrayList();
      for(Residence r : all){
    	    if(r.tenant == null){
    		  vacant.add(r);
    	    }
        }
	    render(ten, vacant);
     }
     login();

  }
  
  /**
   * makes the residence that the tenant was in vacant
   * allows tenant to move into another residence
   * renders json array to help with ajax calls on the page to update markers
   */
  public static void moveOut()
  {
	  Tenant ten = getCurrentTenant();
	  if(ten.res != null){
		ten.res.tenant = null;
		ten.res.save();
	    ten.res = null;
	    ten.save();
	  }
	  renderJSON(geolocationTenant());
  }
  
  /**
   * takes in @param resid to find correct residence
   * finds currently logged in tenant 
   * makes the tenant the residence tenant reference
   * renders json array to help with ajax calls on the page to update markers
   */
  public static void moveIn(Long resid)
  { 
	  Residence resi = Residence.findById(resid);
	  if(resi != null){
	  Tenant ten     = getCurrentTenant();
	  resi.tenant = ten;
	  resi.save();
	  ten.res = resi;
	  ten.save();
	  }
	  renderJSON(geolocationTenant());
  }
 
  /**
   * returns a json array to accomidate the ajax used 
   * to populate the markers on the map on configuration page
   */
  public static JSONArray geolocationTenant()
  {
	  List<Residence> res = Residence.findAll();
	  JSONArray thing = new JSONArray();
	  for(Residence r : res){
		  if(r.tenant == null){
			String tenant = "Vacant";
			String eircode = r.eircode;
			String lat = String.valueOf(r.getGeolocation().getLatitude());
			String lng = String.valueOf(r.getGeolocation().getLongitude());
		    String id = String.valueOf(r.id);
			String Landlord = r.landlord.firstName + r.landlord.lastName;
			String date = String.valueOf(r.date);
			String rent = String.valueOf(r.rent);
			String bedrooms = String.valueOf(r.numberBedrooms);
			String bathrooms = String.valueOf(r.numberBathrooms);
			String area = String.valueOf(r.area);
			String[] list = {eircode, lat, lng, tenant, id, Landlord, date,
					rent, bedrooms, bathrooms, area};
			thing.add(list);
		  } 
	  }
	  renderJSON(thing);
	  return thing;
  }
  
  /**
   * registers a new tenant
   * returns to admin.config as only the admin can register a new tenant
   * @param firstName
   * @param lastName
   * @param email
   * @param password
   * @param terms
   */
  public static void register(String firstName, String lastName, String email, String password, boolean terms)
  {
	  Logger.info(firstName + " " + lastName + " " + email + " " + password);
	  Tenant ten = new Tenant (firstName, lastName, email, password);
	  ten.save();
      Admins.config();
  }

 /**
  * checks that the details put into login page are correct
  * allows user to log in
  * @param email
  * @param password
  */
  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);
    Tenant ten = Tenant.findByEmail(email);
    if ((ten != null) && (ten.checkPassword(password) == true))
    {
      Logger.info("Tenant Authentication successful");
      session.put("logged_in_tenid", ten.id);
      config();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
  
  /**
   * returns current tenant. 
   * helper method.
   * @return
   */
  public static Tenant getCurrentTenant()
  {
	  String tenId = session.get("logged_in_tenid");
	  if(tenId != null)
	  {
		  Tenant logged_in_ten = Tenant.findById(Long.parseLong(tenId));
		  return logged_in_ten;
	  }
	  return null;
  }

}