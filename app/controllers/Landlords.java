package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Landlords extends Controller
{
  /**
   * renders sign up page for landlord
   */
  public static void signup()
  {
	  render();
  }
  
  /**
   * renders login page
   */
  public static void login()
  {
    render();
  }

  /**
   * logs out logged in landlord
   */
  public static void logout()
  {
    session.remove("logged_in_lordid");
    welcome();
  }
  
  /**
   * renders the welcome page
   */
  public static void welcome()
  {
    render();
  }
  
  /**
   * renders the edit page
   */
  public static void edit()
  {
	  Landlord user = getCurrentLord();
	  
	  render(user);
  }

  /**
   * renders config page. if Landlords is logged in otherwise reroutes to login page
   * adds lists of Landlords and Residences to the page
   */
  public static void config()
  {
	 Landlord lord = getCurrentLord();
	  if(lord != null){
	    ArrayList<Residence> residences = new ArrayList();
	    List<Residence> allRes = Residence.findAll();
	    for(Residence r : allRes){
		  if(lord == r.landlord){
		    residences.add(r);
		  }
	    }
	    render(residences, lord);
	  }
	  login();
	}
  
  /**
   * deleted residence, uses resid to find correct residence
   * @param resid
   */
  public static void deleteRes(Long resid)
  {
	   Residence res = Residence.findById(resid);
	   res.delete();
	   config();
  }
  
  /**
   * signs up a new landlord useing
   * returns to admins config page as only the admin can register a landlord
   * @param firstName
   * @param lastName
   * @param email
   * @param password
   * @param address
   * @param terms
   */
  public static void register(String firstName, String lastName, String email, String password, String address, boolean terms)
  {
	  Logger.info(firstName + " " + lastName + " " + email + " " + password);
	  Landlord lord = new Landlord (firstName, lastName, email, password, address);
	  lord.save();
      Admins.config();
  }
  
  /**
   * checks that the details put into login page are correct
   * allows user to log in
   * @param email
   * @param password
   */
  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    Landlord lord = Landlord.findByEmail(email);
    if ((lord != null) && (lord.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_lordid", lord.id);
      config();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
  
  /**
   * @return the landlord logged in if any is available
   */
  public static Landlord getCurrentLord()
  {

	  String lordId = session.get("logged_in_lordid");
	  if(lordId != null)
	  {
		  Landlord logged_in_lord = Landlord.findById(Long.parseLong(lordId));
		  return logged_in_lord;
  	  }
	  return null;
      }
  
  /**
   * used on landlords/edit page to check each field. 
   * if a field is empty is will not change the current value in database
   * @param userid
   * @param firstName
   * @param lastName
   * @param email
   * @param password
   * @param address
   */
  public static void editProfile(Long userid, String firstName, String lastName, String email, String password, String address)
  {
    Landlord editLord = Landlord.findById(userid);
    
    if(firstName != editLord.firstName && firstName.length() > 0)
    {
    	editLord.firstName = firstName;
    }
    else
    {
    	editLord.firstName = editLord.firstName;
    }
    
    if(lastName != editLord.lastName && lastName.length() > 0)
    {
    	editLord.lastName = lastName;
    }
    else
    {
    	editLord.lastName = editLord.lastName;
    }
    
    if(email != editLord.email && email.length() > 0)
    {
    	editLord.email = email;
    }
    else
    {
    	editLord.email = editLord.email;
    }
    
    if(password != editLord.password && password.length() > 0)
    {
    	editLord.password = password;
    }
    else
    {
    	editLord.password = editLord.password;
    }
    
    if(address != editLord.address && address.length() > 0)
    {
        editLord.address = address;
    }
    else
    {
    	editLord.address = editLord.address;
    }
    editLord.save();
    
    config(); 
  }
}