package controllers;
import java.util.Comparator;

import models.Residence;

public class rentSortDecending implements Comparator<Residence>
{
	 /**
	   * Sorts the Residences in parameters by rent variable in descending order
	   */
	@Override
  public int compare(Residence a, Residence b)
  {	 
      return Integer.compare (b.rent, a.rent);
  }
}