package controllers;

import play.*;
import play.mvc.*;
import utils.LatLng;

import java.util.*;

import javax.persistence.ManyToOne;

import models.*;

public class InputData extends Controller
{
  /**
   * renders input data index page if logged in landlord available
   */
  public static void index()
  {
	  Landlord lord = Landlords.getCurrentLord();
		if(lord == null)
		{
			Logger.info("Unable to find current user");
			Landlords.login();
		}
		else
		{
			render(lord);
		}
  }
  
  /**
   * renders edit page for residence
   * @param resid
   */
  public static void edit(Long resid)
  {
	  Residence res = Residence.findById(resid);
	  render(res);
  }
  
  /**
   * allows user to edit specific fields of a residence
   * @param resid
   * @param rent
   */
  public static void editRes(Long resid, int rent)
  {
	  Residence res = Residence.findById(resid);
	  if(rent != res.rent && rent != 0)
	  {
		  res.rent = rent;
	  }
	  else
	  {
		  res.rent = res.rent;
	  }
	  res.save();
	  Landlords.config();
  }
  
  /**
   * method is used when registering a new residence
   * user inputs data and form sends data to this method
   * @param geolocation
   * @param rent
   * @param numberBedrooms
   * @param residenceType
   * @param numberBathrooms
   * @param area
   */

  public static void dataCapture(Landlord lordid, String geolocation, String eircode, int rent, 
		  int numberBedrooms, String residenceType, int numberBathrooms, int area)
  {
	  Landlord lord = Landlords.getCurrentLord();
	  Logger.info(lord + "");
	  addResidence(lord, geolocation, eircode, rent, numberBedrooms, residenceType, numberBathrooms, area);
		
	  index();
  }	
  
  /**
   * helper method for dataCapture()
   * adds a new residence to the database
   * @param lord
   * @param ten
   * @param geolocation
   * @param rent
   * @param numberBedrooms
   * @param residenceType
   * @param numberBathrooms
   * @param area
   */
  private static void addResidence(Landlord lord, String geolocation, String eircode, int rent, 
		  int numberBedrooms, String residenceType, int numberBathrooms, int area)
  {
	  Residence res = new Residence(lord, geolocation, eircode, rent, numberBedrooms, 
			  residenceType, numberBathrooms, area);
	  res.save();
  }
}